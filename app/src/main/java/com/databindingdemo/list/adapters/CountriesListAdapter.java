package com.databindingdemo.list.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.databindingdemo.R;
import com.databindingdemo.databinding.ListviewRowCountriesBinding;

import com.databindingdemo.list.models.Country;

import java.util.ArrayList;

public class CountriesListAdapter  extends RecyclerView.Adapter<CountriesListAdapter.Holder> {

    private Context mContext;
    private ArrayList<Country> mList;

    public CountriesListAdapter(Context context, ArrayList<Country> countriesArrayList) {
        this.mContext = context;
        this.mList = countriesArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        ListviewRowCountriesBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.listview_row_countries, parent, false);
        return new Holder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Country country = mList.get(position);
        holder.bind(country);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        private ListviewRowCountriesBinding mBinding;
        public Holder(ListviewRowCountriesBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
            mBinding.setViewModel(new Country());
        }

        public void bind(Country country) {
            mBinding.setViewModel(country);
            mBinding.executePendingBindings();
        }
    }
}

