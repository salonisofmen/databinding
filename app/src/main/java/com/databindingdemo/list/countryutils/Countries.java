package com.databindingdemo.list.countryutils;

import java.util.ArrayList;

/**
 * Created by sgupta on 9/6/17.
 */

public class Countries {
    private ArrayList<CountriesInfo> info;

    public ArrayList<CountriesInfo> getInfo() {
        return info;
    }

    public void setInfo(ArrayList<CountriesInfo> info) {
        this.info = info;
    }
}
