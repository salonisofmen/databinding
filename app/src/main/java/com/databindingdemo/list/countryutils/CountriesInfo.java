package com.databindingdemo.list.countryutils;

import java.util.ArrayList;

/**
 * Created by sgupta on 9/6/17.
 */

public class CountriesInfo {
    private String countryName;
    private String countryIso;
    private String countryCode;
    private ArrayList<CountriesMediaInfoList> mediaInfo;

    public ArrayList<CountriesMediaInfoList> getMediaInfo() {
        return mediaInfo;
    }
    public void setMediaInfo(ArrayList<CountriesMediaInfoList> mediaInfo) {
        this.mediaInfo = mediaInfo;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryIso() {
        return countryIso;
    }

    public void setCountryIso(String countryIso) {
        this.countryIso = countryIso;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
