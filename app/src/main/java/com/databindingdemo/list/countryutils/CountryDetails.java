package com.databindingdemo.list.countryutils;

import android.content.Context;
import android.util.Log;

import com.databindingdemo.list.models.Country;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class CountryDetails {

    Context mContext;
    private ArrayList<Country> arrayList;

    public CountryDetails(Context context) {
        this.mContext = context;
        arrayList = new ArrayList<>();
        getCountriesList();
    }

    private void getCountriesList(){
        InputStream is = null;
        try {
            is = mContext.getAssets().open("countries.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer);
            Gson gson = new Gson();
            Countries countries = gson.fromJson(json, Countries.class);
            for(CountriesInfo con : countries.getInfo()){
                Country country = new Country();
                country.setCountryName(con.getCountryName());
                country.setCountryIso(con.getCountryIso());
                country.setCountryCode(con.getCountryCode());
                country.setCountryFlag(con.getMediaInfo().get(0).getUrl());
                arrayList.add(country);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Country> getCountryList() {
        return  arrayList;
    }

}
