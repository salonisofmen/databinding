package com.databindingdemo.list.countryutils;

/**
 * Created by sgupta on 9/6/17.
 */

public class CountriesMediaInfoList {
    private String width;
    private String height;
    private String url;

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
