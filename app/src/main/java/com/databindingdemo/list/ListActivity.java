package com.databindingdemo.list;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.databindingdemo.R;
import com.databindingdemo.databinding.ActivityCountiresBinding;
import com.databindingdemo.list.adapters.CountriesListAdapter;
import com.databindingdemo.list.countryutils.CountryDetails;
import com.databindingdemo.list.models.Country;

import java.util.ArrayList;

public class ListActivity extends Activity {

    private ArrayList<Country> list;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCountiresBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_countires);

        list = new ArrayList<>();

        CountryDetails countryDetails = new CountryDetails(this);
        list.addAll(countryDetails.getCountryList());

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        binding.recyclerView.setAdapter(new CountriesListAdapter(this, list));


        Log.d("size is " , " list size  " + list.size());


    }
}
