package com.databindingdemo.signup.custom_adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.databinding.BindingAdapter;
import androidx.databinding.BindingConversion;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseMethod;

import com.databindingdemo.signup.models.UserDetails;

public class BindingAdapters {

    @BindingConversion
    public static int convertBooleanToVisibility(boolean visible) {
        return visible ? View.GONE : View.VISIBLE;
    }

    @BindingAdapter(value = {"errorMessage","error"}, requireAll = false)
    public static void setButton(View view, String message, boolean error) {
        if (error) {
            view.setBackgroundColor(Color.parseColor("#33999999"));
        } else {
            view.setBackgroundColor(Color.parseColor("#ffff4444"));
        }

        if (message!=null && message.length()>0) {
            Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @BindingAdapter("gender")
    public static void setGender(RadioButton radioButton, String text) {
        if (text!=null)
        if (text.equalsIgnoreCase(radioButton.getText().toString())) {
            radioButton.setChecked(true);
        } else {
            radioButton.setChecked(false);
        }
    }

    @InverseBindingAdapter(attribute = "gender", event = "genderAttrChanged")
    public static String  getGender(RadioButton radioButton) {
        return radioButton.getText().toString();
    }

    @BindingAdapter(value = "genderAttrChanged")
    public static void setListener(RadioButton radioButton, final InverseBindingListener listener) {
        Log.d("called method", "setListener " + radioButton.getText());

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    listener.onChange();
                }
            }
        });
    }

    @InverseMethod("booleanToString")
    public static boolean stringToBoolean(RadioButton rb1, RadioButton rb2, String text) {
        if (rb1.getText().toString().equalsIgnoreCase(text)) {
            //if not included in radio group
            rb2.setChecked(false);
            return true;
        }
        return false;
    }

    public static String booleanToString(RadioButton rb1, RadioButton rb2, boolean checked) {
        if (rb1.isChecked()) {
            //if not included in radio group
            rb2.setChecked(false);
            return rb1.getText().toString();
        } else if(rb2.isChecked()) {
            //if not included in radio group
            rb1.setChecked(false);
            return rb2.getText().toString();
        } else {
            return "";
        }
    }


    @InverseMethod("getCountryNameFromPosition")
    public static int getCountryPositionFromName(String countryName) {
        if (countryName!=null) {
            for (int i = 0; i< UserDetails.countryArray.length; i++) {
                if (countryName.equalsIgnoreCase(UserDetails.countryArray[i])) {
                    return i;
                }
            }
        }
        return 1;
    }


    public static String getCountryNameFromPosition(int position) {
        return UserDetails.countryArray[position];
    }

    @InverseBindingAdapter(attribute = "bind:setSelectedItem")
    public static int getSelectedItem(View view) {
        return  ((Spinner)view).getSelectedItemPosition();
    }

    @BindingAdapter(value = {"bind:setSelectedItem","setSelectedItemAttrChanged"}, requireAll = false)
    public static void onItemSelected(Spinner spinner, int position, final InverseBindingListener listener) {
        if (spinner.getSelectedItemPosition()!=position) {

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    listener.onChange();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            spinner.setSelection(position);

        }
    }
}
