package com.databindingdemo.signup.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import com.databindingdemo.R;
import com.databindingdemo.databinding.BindingSignUpActivity;
import com.databindingdemo.signup.handlers.ClickHandlers;
import com.databindingdemo.signup.models.UserDetails;


public class SignUpActivity extends Activity {

    private UserDetails userDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userDetails = new UserDetails();
        BindingSignUpActivity bindingSignUpActivity = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        bindingSignUpActivity.setUserDetails(userDetails);

        bindingSignUpActivity.setClickHandlers(new ClickHandlers());
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item, userDetails.countryArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bindingSignUpActivity.spinner.setAdapter(aa);

        bindingSignUpActivity.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
