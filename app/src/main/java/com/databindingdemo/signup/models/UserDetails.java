package com.databindingdemo.signup.models;

import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;

import androidx.core.util.PatternsCompat;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;


public class UserDetails extends BaseObservable {
    public static String[] countryArray = { "Select Country", "India", "USA", "China", "Japan", "Other"};

    @Bindable
    private String errorMessage;
    @Bindable
    private boolean error = true;

    private String firstName;
    private String middleName;
    private String lastName;
    private String email;
    private String phone;
    private String dob;
    private String gender;
    private String country;

    private boolean haveAMiddleName = true;

    @Bindable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(BR.firstName);
    }

    @Bindable
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
        notifyPropertyChanged(BR.middleName);

    }

    @Bindable
    public boolean isHaveAMiddleName() {
        return haveAMiddleName;
    }

    public void setHaveAMiddleName(boolean haveAMiddleName) {
        this.haveAMiddleName = haveAMiddleName;
        notifyPropertyChanged(BR.haveAMiddleName);
    }

    @Bindable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(BR.lastName);

    }

    public boolean getError() {
        return error;
    }

    private void setError(boolean error) {
        this.error = error;
        notifyPropertyChanged(BR.error);
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        notifyPropertyChanged(BR.errorMessage);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(BR.phone);
    }

    @Bindable
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
        notifyPropertyChanged(BR.dob);
    }

    @Bindable
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
        notifyPropertyChanged(BR.gender);
    }

    @Bindable
    public String getCountry() {
        return country;
    }


    public void setCountry(int index) {
        if (index==0) {
            this.country = "";
            notifyPropertyChanged(BR.country);
        } else {
            this.country = countryArray[index];
            notifyPropertyChanged(BR.country);
        }
    }

    public void setCountry(String index) {

            this.country = index;
            notifyPropertyChanged(BR.country);

    }

    public void onClick (View view) {

        boolean isError = false;
        String message = "";
        if (!isStringValid(firstName)) {
            isError = true;
            message = "Invalid First Name";
        } else if (haveAMiddleName && !isStringValid(middleName) ) {
            isError = true;
            message = "Invalid Middle Name";
        } else if (!isStringValid(lastName)) {
            isError = true;
            message = "Invalid Last Name";
        } else if (!isStringValid(email) || !PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()) {
            isError = true;
            message = "Invalid Email";
        } else if (!isStringValid(phone)) {
            isError = true;
            message = "Invalid Phone";
        } else if (!isStringValid(dob)) {
            isError = true;
            message = "Select Dob";
        } else if (!isStringValid(gender)) {
            isError = true;
            message = "Select Gender";
        } else if (!isStringValid(country)) {
            isError = true;
            message = "Select Country";
        } else {
            message= "Success";
        }
        setError(isError);
        setErrorMessage(message);

    }

    private boolean isStringValid(String s) {
        return s!=null && s.trim().length()>0;
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.isChecked()) {
            gender = buttonView.getText().toString();
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        this.country = countryArray[position];
        notifyPropertyChanged(BR.country);
    }


}
