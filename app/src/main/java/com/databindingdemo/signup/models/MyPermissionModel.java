package com.databindingdemo.signup.models;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.library.baseAdapters.BR;

public class MyPermissionModel extends BaseObservable {


    private boolean permission1;
    private boolean permission2= true;
    private boolean  permission3 = true;
    private boolean permission4;
    private boolean permission5 = true;
    private boolean permission6;


    private String selectedText = null;


    @Bindable
    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
        notifyPropertyChanged(BR.selectedText);
    }

    @Bindable
    public boolean isPermission1() {
        return permission1;
    }

    public void setPermission1(boolean permission1) {
        this.permission1 = permission1;
        notifyPropertyChanged(BR.permission1);
    }

    @Bindable
    public boolean isPermission2() {
        return permission2;
    }

    public void setPermission2(boolean permission2) {
        this.permission2 = permission2;
        notifyPropertyChanged(BR.permission2);
    }

    @Bindable
    public boolean isPermission3() {
        return permission3;
    }

    public void setPermission3(boolean permission3) {
        this.permission3 = permission3;
        notifyPropertyChanged(BR.permission3);
    }

    @Bindable
    public boolean isPermission4() {
        return permission4;
    }

    public void setPermission4(boolean permission4) {
        this.permission4 = permission4;
        notifyPropertyChanged(BR.permission4);
    }

    @Bindable
    public boolean isPermission5() {
        return permission5;
    }

    public void setPermission5(boolean permission5) {
        this.permission5 = permission5;
        notifyPropertyChanged(BR.permission5);
    }

    @Bindable
    public boolean isPermission6() {
        return permission6;
    }

    public void setPermission6(boolean permission6) {
        this.permission6 = permission6;
        notifyPropertyChanged(BR.permission6);
    }

    @BindingAdapter(value = {"bind:hasPermission","bind:model"})
    public static void setRadioButtonPermission(RadioButton radioButton, boolean permission, String selectedText){

        if (permission) {
            radioButton.setVisibility(View.VISIBLE);
        } else {
            radioButton.setVisibility(View.GONE);
        }

        if (permission && selectedText == null) {
            radioButton.setChecked(true);

        }
    }


    @InverseBindingAdapter(attribute = "bind:model", event = "bind:modelAttrChanged")
    public static String  getGender(RadioButton radioButton) {
        if (radioButton.isChecked()) {
            return radioButton.getText().toString();
        } else {
            return null;
        }
    }


    @BindingAdapter(value = "bind:modelAttrChanged")
    public static void setListener(RadioButton radioButton, final InverseBindingListener listener) {

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    listener.onChange();
                }
            }
        });

    }

}
