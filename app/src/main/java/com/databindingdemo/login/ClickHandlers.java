package com.databindingdemo.login;

import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.databindingdemo.login.models.LoginModel;
import com.databindingdemo.login.models.LoginModelWithObservable;
import com.databindingdemo.login.models.LoginModelWithObservableFields;

public class ClickHandlers {

    private String email ="salonimit1@mgial.com";

    public void onLoginClick(View view, LoginModel model) {
        model.setEmail(email);
    }

    public void onLoginClickWithFields(View view, LoginModelWithObservableFields model ) {
        LoginModelWithObservableFields.email.set(email);
        //model.email.set(email);
    }

    public void onLoginClickWithObservable(View view, LoginModelWithObservable model ) {
        model.setEmail(email);
    }

    public void onCollectionButtonClick(Context context, String name, String email) {
        Toast.makeText(context, "name is : " + name +" & email is: " + email, Toast.LENGTH_SHORT).show();
    }



}

