package com.databindingdemo.login.activities;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.databindingdemo.R;
import com.databindingdemo.databinding.ActivityLoginWithObservableBinding;
import com.databindingdemo.login.ClickHandlers;
import com.databindingdemo.login.models.LoginModelWithObservable;

public class LoginActivityWithObservable extends Activity {

    public static final String LOG_TAG = LoginActivityWithObservable.class.getSimpleName();
    private LoginModelWithObservable model;
    private ClickHandlers clickHandlers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityLoginWithObservableBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login_with_observable);
        model = new LoginModelWithObservable();
        clickHandlers = new ClickHandlers();
        activityLoginBinding.setLoginDetails(model);
        activityLoginBinding.setHandler(clickHandlers);


    }
}
