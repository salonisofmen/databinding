package com.databindingdemo.login.activities;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableArrayMap;

import com.databindingdemo.R;
import com.databindingdemo.databinding.ActivityLoginUsingObservableListBinding;
import com.databindingdemo.databinding.ActivityLoginUsingObservableMapBinding;
import com.databindingdemo.login.ClickHandlers;

public class LoginActivityUsingCollections extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bindUsingMap();
    }

    private void bindUsingArray() {
        ObservableArrayList<String> user = new ObservableArrayList<>();
        user.add("Saloni");
        user.add("saloimit1@gmail.com");

        ActivityLoginUsingObservableListBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login_using_observable_list);
        binding.setHandler(new ClickHandlers());
        binding.setUserDetails(user);
    }


    private void bindUsingMap() {

        ObservableArrayMap<String,String> user = new ObservableArrayMap<>();
        user.put("name", "Saloni");
        user.put("email", "saloimit1@gmail.com");

        ActivityLoginUsingObservableMapBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login_using_observable_map);
        binding.setHandler(new ClickHandlers());
        binding.setUserDetails(user);
    }
}
