package com.databindingdemo.login.activities;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.databindingdemo.R;
import com.databindingdemo.databinding.ActivityLoginBinding;
import com.databindingdemo.login.ClickHandlers;
import com.databindingdemo.login.models.LoginModel;

public class LoginActivity extends Activity {

    public static final String LOG_TAG = LoginActivity.class.getSimpleName();
    private LoginModel model;
    private ClickHandlers clickHandlers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        model = new LoginModel();
        clickHandlers = new ClickHandlers();
        activityLoginBinding.setLoginDetails(model);
        activityLoginBinding.setHandler(clickHandlers);

        model.setEmail("fkjhssj");

    }
}
