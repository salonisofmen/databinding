package com.databindingdemo.login.models;

import androidx.databinding.ObservableField;

public class LoginModelWithObservableFields {

    //By the way data binding handles the null pointer exception but observables fields ,
    // collections (maps and arrays) need to be initialized


    /*ObservableBoolean
ObservableByte
ObservableChar
ObservableShort
ObservableInt
ObservableLong
ObservableFloat
ObservableDouble
ObservableParcelable*/
    public static ObservableField<String> email = new ObservableField<>();
    private String password;

    public ObservableField<String> getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}

